fn main() {
    let mut number:u32 = 1;
    loop {
        number += 1;
        let mut success = true;
        for i in 2..20 {
            if number % i != 0 {
                success = false;
                break;
            }
        }
        if success {
            println!("{}",number);
            break;
        }
    }
}

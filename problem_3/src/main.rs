fn main() {
    let number:u64 = 600851475143;
    let mut largest_prime:u64 = 1;
    let mut prime = 1;
    for i in 2..number {
        if number % i == 0 {
            if prime == 0 {
                break;
            } else {
                for j in 2..i {
                    if i % j == 0 {
                        prime = 0;
                        break;
                    }
                }
                if prime == 1 {
                    largest_prime = i;
                }
            }
        }
    }
    println!("Largest Prime Factor of {} is {}",number,largest_prime);
}

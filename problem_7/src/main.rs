fn main() {
    let mut number:u32 = 2;
    let mut counter:u16 = 0;
    loop {
        let mut prime:bool = true;
        for i in 2..number {
            if number % i == 0 {
                prime = false;
                break;
            }
        }
        if prime {
            counter +=1;
            if counter == 10001 {
                println!("{}",number);
                break;
            }
        }
        number += 1;
    }
}
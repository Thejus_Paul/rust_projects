fn main() {
    let mut first = 1;
    let mut second = 2;
    let mut sum = second;
    loop {
        let third = first + second;
        if third < 4_000_000 {
            if third % 2 == 0 {
                sum += third;
            }
        }
        else {
            break;
        }
        first = second;
        second = third;
    }
    print!("Sum of Even-Valued Terms is {}",sum);
}
fn main() {
    let mut max_palindrome = 0;
    for i in 100..1000 {
        for j in 100..1000 {
            let mut palindrome = 0;
            let mut product = i * j;
            while product > 0 {
                palindrome = (palindrome*10) + (product%10);
                product /= 10;
            }
            if (palindrome == (i*j)) && (palindrome > max_palindrome)  {
                max_palindrome = palindrome;
            }
        }
    }
    println!("The largest palindrome made from the product of two 3-digit numbers is {}",max_palindrome);
}
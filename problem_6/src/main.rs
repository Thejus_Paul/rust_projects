fn main() {
    let mut sum_of_sq:usize = 0;
    let mut sq_of_sum:usize = 0;
    for i in 1..101 {
        sum_of_sq += i*i;
        sq_of_sum += i;
    }
    sq_of_sum = sq_of_sum*sq_of_sum;
    let difference = sq_of_sum - sum_of_sq;
    println!("Difference is {} - {} = {}",sq_of_sum,sum_of_sq,difference);
}
